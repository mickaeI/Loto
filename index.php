<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <title>Loto</title>
</head>

<body>
    <section class="bg-danger text-white">
        <article class="container">
            <form method="POST" action="#" id="formRegles">
                <br>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Nombre de tirages</span>
                    </div>
                    <input type="text" name="nbTirages" id="nbTirages" required placeholder="Nombre de tirages" class="form-control">
                </div>
                <br>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Numéros joués</span>
                    </div>
                    <input type="text" name="numero1" id="numerosJoues" required placeholder="numéro 1" class="form-control">
                    <input type="text" name="numero2" id="numerosJoues" required placeholder="numéro 2" class="form-control">
                    <input type="text" name="numero3" id="numerosJoues" required placeholder="numéro 3" class="form-control">
                    <input type="text" name="numero4" id="numerosJoues" required placeholder="numéro 4" class="form-control">
                    <input type="text" name="numero5" id="numerosJoues" required placeholder="numéro 5" class="form-control">  
                </div>
                <br>
            </form>
            <button type="submit" class="btn btn-success" form="formRegles">Valider</button>
            <br><br>
        </article>
    </section>

    <?php
        // Création de la liste des nombres possibles :
        $min = 1;
        $max = 49;
        $tailleTirage = 5;
        $nbTirages = $_POST['nbTirages'];

        var_dump($nbTirages);

        // Création du tableau des tirages :
        function XNombresAleatoiresUniques($min, $max, $tailleTirage) {
            $loto = range($min, $max);
            shuffle($loto);
            return array_slice($loto, 0, $tailleTirage);
        }

        // Faire 10 tirages :
        for ($i=0; $i<$nbTirages; $i++){
            $tirage[$i] = XNombresAleatoiresUniques($min, $max, $tailleTirage);
        }
        echo '<br><br><h1>Les tirages sont les suivants : </h1>';
        var_dump($tirage);

        // Fusion des tirages afin d'effectuer un classement :
        $tirageReunis = array_merge($tirage[0], $tirage[1], $tirage[2], $tirage[3], $tirage[4], $tirage[5], $tirage[6], $tirage[7], $tirage[8], $tirage[9]);
        // Classement des nombres les plus représentés :
        $classement = array_count_values($tirageReunis);
        arsort($classement);
        echo '<br><br><h1>Nombres les plus representés dans les tirages : </h1>';
        var_dump($classement);

        // Le joueur joue les numéros suivants : 
        $numero1 = $_POST['numero1'];
        $numero2 = $_POST['numero2'];
        $numero3 = $_POST['numero3'];
        $numero4 = $_POST['numero4'];
        $numero5 = $_POST['numero5'];

        $combinaisonJouee = array($numero1, $numero2, $numero3, $numero4, $numero5);
        echo '<br><br><h1>Vous avez joué la combinaison suivante : </h1>';
        var_dump($combinaisonJouee);

        function testerCombinaisonDesordre($combinaisonJouee, $tirage){
            for ($j=0;$j<10;$j++){
                $nombresEnCommun = array_intersect($combinaisonJouee, $tirage[$j]);
                echo '<br><br><h2>Nombres présents dans la combinaison gagnante : </h2>';
                var_dump(count($nombresEnCommun));
                if (count($nombresEnCommun) == 5){
                    echo "C'est gagné !";
                }
                else {
                    echo "C'est perdu !";
                }
            }
        }
        testerCombinaisonDesordre($combinaisonJouee, $tirage);
        // Tester les numéros joués : TO BE CONTINUED...
        //echo($_POST['numero']);
        //$numerosjoues = $_POST('numero');
        //var_dump($numerosjoues);
    ?>

</body>

</html>